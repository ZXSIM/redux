import { FETCH_POSTS, NEW_POST } from './type';

export const fetchPosts = () => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(results=> results.json())
    .then(post=> dispatch({
        type: FETCH_POSTS,
        payload: post
    }));

}

export const createPost = postData => dispatch => {
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify(postData)
    })
    .then(response => response.json())
    .then(post => dispatch({
        type: NEW_POST,
        payload: post
    }))

}