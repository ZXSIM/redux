import { FETCH_POSTS, NEW_POST } from '../Actions/type';

const initialState = {
    items: [], //represent the post that comes in from Action
    item: {},
}

export default function(state = initialState, action){
    switch(action.type){
        case FETCH_POSTS:
            return {
                ...state,
                items: action.payload
            };
        case NEW_POST:
            return {
                ...state,
                item: action.payload
            };
        default:
            return state;
    }
}