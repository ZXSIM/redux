import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { fetchPosts } from '../Store/Actions/postActions'

class Posts extends Component {
    constructor(props){
        super(props);

        this.props.fetchPosts();
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.newPost){
            this.props.post.unshift(nextProps.newPost)
        }
    }

  render() {
    const postItems = this.props.post.map(post => (
        <div key={post.id}>
            <h3>{post.title}</h3>
            <p>{post.body}</p>
        </div>
    ));
    return (
      <div>
        <h1>Posts</h1>
        {postItems}
      </div>
    )
  }
}

Posts.propTypes = {
    fetchPosts: PropTypes.func.isRequired,
    post: PropTypes.array.isRequired,
    newPost: PropTypes.object
}

const mapStateToProps = state => {
    return ({
    post: state.posts.items,
    newPost: state.posts.item
});
}

export default  connect(mapStateToProps, {fetchPosts})(Posts);
